import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import ReactTable from 'react-table-6'
import 'react-table-6/react-table.css'
import DateFnsUtils from '@date-io/date-fns';
import 'date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DeleteIcon from '@material-ui/icons/Delete';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="#">
                SMS
        </Link>
            {' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(3),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    formControl: {
        margin: theme.spacing(0),
        minWidth: 190,
    },
    searchButton: {
        marginTop: '30px',
    }
}));

export default function SignUp() {
    const classes = useStyles();
    const [files, setFiles] = useState([]);
    const [cities, setCities] = useState([]);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());

    const cityColumns = [
        {
            Header: 'City',
            accessor: 'city'
        },
        {
            Header: 'Start Date',
            accessor: 'start_Date',
            Cell: row => (
                <div>
                    {row.value !== null ? new Date(row.value).toLocaleDateString() : null}
                </div>
            )
        },
        {
            Header: 'End Date',
            accessor: 'end_Date',
            Cell: row => (
                <div>
                    {row.value !== null ? new Date(row.value).toLocaleDateString() : null}
                </div>
            )
        },
        {
            Header: 'Price',
            accessor: 'price'
        },
        {
            Header: 'Status',
            accessor: 'status'
        },
        {
            Header: 'Color',
            accessor: 'color',
            sortable: true
        },
        {
            accessor: 'edit',
            filterable: false,
            resizable: false,
            sortable: false,
            width: 80,
            Cell: row => (
                <div>
                    <DeleteIcon color={"primary"} aria-label="Delete" titleAccess='Delete city' onClick={() => handleDelete(row.original.id)} />
                </div>
            )
        }
    ];

    const handleStartDateChange = (date) => {
        setStartDate(date);
    };

    const handleEndDateChange = (date) => {
        setEndDate(date);
    };

    const handleSearch = () => {
        let start_date = new Date(startDate).toLocaleDateString('fr-CA');
        let end_date = new Date(endDate).toLocaleDateString('fr-CA');

        const response = axios.get(`http://localhost:5000/city/${start_date}/${end_date}`);
        response.then((res) => {
            setCities(res.data);
        }).catch((error) => {
            console.log(error)
        });
    }

    const handleDelete = (id) => {
        const response = axios.delete(`http://localhost:5000/city/${id}`);
        response.then((res) => {
            alert('City details deleted');
            fetchData();
        }).catch((error) => {
            console.log(error)
        });
    }

    React.useEffect(() => {
        fetchData();
    }, []);

    const fetchData = () => {
        const response = axios.get('http://localhost:5000/city/');
        response.then((res) => {
            setCities(res.data);
        }).catch((error) => {
            console.log(error)
        });
    }

    const handleChange = e => {
        const fileReader = new FileReader();
        fileReader.readAsText(e.target.files[0], "UTF-8");
        fileReader.onload = e => {
            let objCity = [];
            JSON.parse(e.target.result).forEach(item => {
                objCity.push({
                    city: item.city,
                    color: item.color,
                    start_date: new Date(item.start_date).toLocaleDateString('fr-CA'),
                    end_date: new Date(item.end_date).toLocaleDateString('fr-CA'),
                    price: parseFloat(item.price),
                    status: item.status
                });
            });
            setFiles(objCity);
        };
    };

    const handleSeedDatabase = () => {
        const response = axios.post('http://localhost:5000/city/', files);
        response.then((res) => {
            setCities(res.data);
        }).catch((error) => {
            console.log(error)
        });
    }

    return (
        <React.Fragment>
            <Container component="main" maxWidth="xl">

                <div className={classes.paper}>
                    <Grid container spacing={2} justify="flex-end" direction="row">
                        <input type="file" accept="application/JSON" onChange={handleChange} />
                        <Button variant="contained" size="small" color="primary" onClick={handleSeedDatabase}>
                            Upload
                        </Button>
                    </Grid>

                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12}>
                            <Typography component="h1" variant="h5">
                                Plants
                            </Typography>
                        </Grid>
                    </Grid>

                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={3}>
                                <KeyboardDatePicker
                                    disableToolbar
                                    variant="inline"
                                    format="MM/dd/yyyy"
                                    margin="normal"
                                    id="date-picker-inline"
                                    label="Date picker inline"
                                    value={startDate}
                                    onChange={handleStartDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <KeyboardDatePicker
                                    disableToolbar
                                    variant="inline"
                                    format="MM/dd/yyyy"
                                    margin="normal"
                                    id="date-picker-inline"
                                    label="Date picker inline"
                                    value={endDate}
                                    onChange={handleEndDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <Button className={classes.searchButton} variant="contained" size="small" color="primary" onClick={handleSearch}>
                                    Search
                                </Button>
                            </Grid>
                        </Grid>
                    </MuiPickersUtilsProvider>

                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12}>
                            <ReactTable
                                data={cities}
                                columns={cityColumns}
                            />
                        </Grid>
                    </Grid>
                </div>
                <Box mt={5}>
                    <Copyright />
                </Box>
            </Container >
        </React.Fragment>
    );
}